
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Starter Template · Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
        <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<main role="main" class="container">

    <div class="starter-template">
        <h1>test API</h1>
        <p>This test is built in an enviroment using DOCKER + apache2.4 + php7.3.3</p>
        <p>Only the API structure was developed.</p>
        <p>Using GIT download the code to your machine here is the link <a href="www.google.com" target="_blanck">link</a></p>
        <p>It will be necessary the composer, npm and docker to run the code.</p>
        <p>After download de code use the comand 'docker-compose up -d' to build the containers on docker</p>
        <p>Then run the comand 'composer install' to install all libraries necessary to a Laravel Application</p>
        <p>And the last is 'npm install'</p>
        <p>With this done, we must to create our table on the database with the command 'php artisan migrate'</p>
        <p>And to populate the table with some fake data we can use 'php artisan db:seed'</p>
        <p>Now using a program like 'Postman' you can access the route 'localhost:92/api/v1/product-data',, to view, delete, insert or update the data of that table.</p>
        <p>Below is a exemple of json to make a teste on 'Postman'.</p>
        <p>{"name":"1234",
            "sku": "112523",
            "type" : "required",
            "description" : "required",
            "product_name" : "required",
            "category" : "required",
            "first_header_cart" : "required",
            "second_header_cart" : "required",
            "original_price" : "10.3",
            "final_price" : "13.0",
            "option" : "ttt",
            "stock" : "4"}</p>
    </div>

</main><!-- /.container -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
</html>
