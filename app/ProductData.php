<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProductData extends Model
{
    /**
     * Model table name
     *
     * @var string
     */
    protected $table = "product_datas";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sku',
        'type',
        'image',
        'description',
        'product_name',
        'category',
        'first_header_cart',
        'second_header_cart',
        'original_price',
        'final_price',
        'stock',
        'option',
        'url',
        'updated_at'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     *  The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function contentSet() {
        return $this->hasOne(ContentSet::class, 'product_data_id', 'id');
    }

    public function attributes() {
        return $this->hasOne(Attributes::class, 'product_data_id', 'id');
    }
}
