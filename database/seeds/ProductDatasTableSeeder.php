<?php

use Illuminate\Database\Seeder;

class ProductDatasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\ProductData', 10)->create();
    }
}
