<?php

use Illuminate\Database\Seeder;

class ContentSetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\ContentSet', 10)->create();
    }
}
