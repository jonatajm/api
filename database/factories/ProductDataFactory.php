<?php

use Faker\Generator as Faker;

$factory->define(\App\ProductData::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'sku' => $faker->unique()->randomNumber(9),
        'type' => 'simple',
        'image' => null,
        'description' => $faker->sentence(),
        'product_name' => $faker->sentence(),
        'category' => $faker->sentence(),
        'first_header_cart' => $faker->sentence(),
        'second_header_cart' => $faker->sentence(),
        'original_price' => $faker->randomFloat(2,0,999),
        'final_price' => $faker->randomFloat(2,0,999),
        'stock' => $faker->numberBetween(0,10),
        'option' => $faker->sentence(),
        'url' => null,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
