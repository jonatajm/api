<?php

use Faker\Generator as Faker;

$factory->define(\App\ContentSet::class, function (Faker $faker) {
    return [
        'text' => 'qwerty',
        'image' => null,
        'product_data_id' => \App\ProductData::first()->id,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
