<?php

namespace App\Traits;


class CollectionFilterTrait
{
    public static function filterRequest($request, $collection) {
        foreach($request->all() as $reqKey => $reqValue) {
            $collection = self::simpleFilterRequest($reqKey, $reqValue, $collection, ['sku','name']);
            $collection = self::relationFilterRequest($reqKey, $reqValue, $collection, ['attributes:size','attributes:tog']);
        }
        return $collection;
    }

    public static function simpleFilterRequest($key, $value, $collection, $arrayFields) {
        if (in_array($key, $arrayFields)) {
            $collection = $collection->filter(function ($value) use($key, $value){
                $arrayVal = explode(',',$value);
                return in_array($value->{$key}, $arrayVal);
            });
        }
        return $collection;
    }

    public static function relationFilterRequest($key, $value, $collection, $arrayFields) {
        if (in_array($key, $arrayFields)) {
            $collection = $collection->filter(function ($value) use ($key, $value) {
                $arrayVal = explode(',', $value);
                $relationKey = explode(':', $key);
                if ($value->attributes) {
                    return in_array($value->attributes->{$relationKey[1]}, $arrayVal);
                }
            });
        }
        return $collection;
    }
}