<?php

use Faker\Generator as Faker;

$factory->define(\App\Attributes::class, function (Faker $faker) {
    return [
        'size' => '260x220',
        'tog' => '10.5 tog',
        'product_data_id' => \App\ProductData::first()->id,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
