<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentSet extends Model
{
    /**
     * Model table name
     *
     * @var string
     */
    protected $table = "content_sets";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_data_id',
        'text',
        'image'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     *  The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function productData() {
        return $this->belongsTo(ProductData::class,'product_data_id','id');
    }
}
