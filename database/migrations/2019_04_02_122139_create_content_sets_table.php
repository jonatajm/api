<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_sets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_data_id');
            $table->string('text')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
            $table->foreign('product_data_id')->references('id')->on('product_datas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_sets');
    }
}
