<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_datas', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name');
            $table->string('sku');
            $table->string('type');
            $table->string('image')->nullable();
            $table->string('description')->nullable();
            $table->string('product_name');
            $table->string('category');
            $table->string('first_header_cart');
            $table->string('second_header_cart');
            $table->float('original_price', 9,2);
            $table->float('final_price', 9,2);
            $table->integer('stock');
            $table->string('option');
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_datas');
    }
}