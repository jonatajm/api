<?php

namespace App\Http\Controllers\V1;

use App\Attributes;
use App\ContentSet;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductDataCollection;
use App\Traits\CollectionFilterTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductData extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productsData = \App\ProductData::all();
        if ($request->all()) {
            $productsData = CollectionFilterTrait::filterRequest($request, $productsData);
        }

        return new ProductDataCollection($productsData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|max:255',
                'sku' => 'required|unique:product_datas',
                'type' => 'required',
                'description' => 'required|max:255',
                'original_price' => 'numeric',
                'final_price' => 'numeric',
                'stock' => 'numeric',
            ]);
            return \App\ProductData::create($request->all());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ProductDataCollection(\App\ProductData::where('id', $id)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => 'required|max:255',
                'sku' => 'required|unique:product_datas',
                'type' => 'required',
                'description' => 'required|max:255',
                'original_price' => 'numeric',
                'final_price' => 'numeric',
                'stock' => 'numeric',
            ]);
            return 'ok';
            return \App\ProductData::update($request->all(), $id);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            \App\ProductData::destroy($id);
            return 'registro excluido';
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
