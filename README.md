###TEST - API

- This test is built in an enviroment using DOCKER + apache2.4 + php7.3.3
- Only the API structure was developed
- Using GIT download the code to your machine here is the link (https://bitbucket.org/jonatajm/api/src/master/ "repository")
- It will be necessary the composer, npm and docker to run the code
- After download de code use the comand 'docker-compose up -d' to build the containers on docker
- Then run the comand 'composer install' to install all libraries necessary to a Laravel Application
- And the last is 'npm install'
- With this done, we must to create our table on the database with the command 'php artisan migrate'
- With this done, we must to create our table on the database with the command 'php artisan migrate'
- And to populate the table with some fake data we can use 'php artisan db:seed'
- Now using a program like 'Postman' you can access the route 'localhost:92/api/v1/product-data', to view, delete, insert or update the data of that table
- Below is a exemple of json to make a teste on 'Postman'
        {
		"name":"BedKing",
        "sku": "112523",
        "type" : "bed",
        "description" : "It is king size 2",
        "product_name" : "king2ize",
        "category" : "bed",
        "first_header_cart" : "test",
        "second_header_cart" : "test",
        "original_price" : "10.3",
        "final_price" : "13.0",
        "option" : "ttt",
        "stock" : "4"
		}